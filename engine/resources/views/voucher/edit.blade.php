@extends('layouts.master')
@section('main_content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit {{$voucher->name}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">voucher</a></li>
                        <li class="breadcrumb-item active">edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{route('voucher.update', $voucher->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="code">Voucher Code</label>
                                    <input type="text" name="code" class="form-control" id="code"
                                        placeholder="Voucher Code" value="{{$voucher->code}}">
                                    @error('code')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">Voucher Description</label>
                                    <input type="text" name="description" class="form-control" id="description"
                                        placeholder="Voucher Description" value="{{$voucher->description}}">
                                    @error('description')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="discount_type">Discount Type</label>
                                    <select class="form-control select2" name="discount_type" id="discount_type">
                                        <option value="AMOUNT" @if ($voucher->discount_type == 'AMOUNT') selected
                                            @endif>Fixed Amount Discount</option>
                                        <option value="PERCENT" @if ($voucher->discount_type == 'PERCENT') selected
                                            @endif>Percentage Amount Discount</option>
                                    </select>
                                    @error('discount_type')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group" id="count-box">
                                    <label for="discount">Discount</label>
                                    <input type="text" name="discount" class="form-control" id="discount"
                                        placeholder="Enter discount" value="{{$voucher->discount}}">
                                    @error('discount')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <label for="expiry_date">Discount End Date</label>
                                        <div class="input-group date" id="timepicker-expiry_date"
                                            data-target-input="nearest" style="max-width: 200px">
                                            <input type="text" name="expiry_date"
                                                class="form-control datetimepicker-input"
                                                value="{{$voucher->expiry_date}}"
                                                data-target="#timepicker-expiry_date" />
                                            <div class="input-group-append" data-target="#timepicker-expiry_date"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                            </div>
                                        </div>
                                        @error('expiry_date')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="item_id">Product</label>
                                    <select class="form-control select2" name="item_id" id="item_id">
                                        <option value="ALLPRODUCT" @if ($voucher->item_id == 'ALLPRODUCT') selected
                                            @endif>Semua Product</option>
                                        @foreach ($items as $item)
                                        <option value="{{$item->id}}" @if ($voucher->item_id == $item->id) selected
                                            @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('item_id')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="max_count">Max Digunakan</label>
                                    <input type="text" name="max_count" class="form-control" id="max_count"
                                        placeholder="Enter max_count" value="{{$voucher->max_count}}">
                                    @error('max_count')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="min_subtotal">Minimal Harga Subtotal</label>
                                    <input type="text" name="min_subtotal" class="form-control" id="min_subtotal"
                                        placeholder="Enter min_subtotal" value="{{$voucher->min_subtotal}}">
                                    @error('min_subtotal')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="max_subtotal">Max Harga Subtotal</label>
                                    <input type="text" name="max_subtotal" class="form-control" id="max_subtotal"
                                        placeholder="Enter max_subtotal" value="{{$voucher->max_subtotal}}">
                                    @error('max_subtotal')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="subtotal_message">Pesan Jiga Kurang Dari Subtotal</label>
                                    <input type="text" name="subtotal_message" class="form-control"
                                        id="subtotal_message" placeholder="Enter subtotal_message"
                                        value="{{$voucher->subtotal_message}}">
                                    @error('subtotal_message')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="is_apps">Tersedian untuk</label>
                                    <select class="form-control select2" name="is_apps" id="is_apps">
                                        <option value="1" @if ($voucher->is_apps == 1) selected @endif>Web</option>
                                        <option value="3" @if ($voucher->is_apps == 3) selected @endif>Aplikasi</option>
                                        <option value="2" @if ($voucher->is_apps == 2) selected @endif>Web & Aplikasi
                                        </option>
                                    </select>
                                    @error('is_apps')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="is_active">Active</label>
                                    <input type="checkbox" class="form-check-input" id="is_active" name="is_active"
                                    @if($voucher->is_active == 1) checked @endif style="margin-left: 1rem;">
                                    @error('is_active')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('js')
<script>
    $( document ).ready(function() {
        $("#discount_type").change()
        $.fn.datetimepicker.Constructor.Default = $.extend({},
            $.fn.datetimepicker.Constructor.Default,
            { icons:{
                time: 'fas fa-clock',
                date: 'fas fa-calendar',
                up: 'fas fa-arrow-up',
                down: 'fas fa-arrow-down',
                previous: 'fas fa-arrow-circle-left'
                ,next: 'fas fa-arrow-circle-right',
                today: 'far fa-calendar-check-o',
                clear: 'fas fa-trash',
                close: 'far fa-times' } });
    });
    $('.select2').select2({
      theme: 'bootstrap4',
    })
    $('#timepicker').datetimepicker({
        format: 'DD-MM-YYYY HH:mm'
    })
    $("#discount_type").change(function(){
        if ($(this).val() == 'PERCENT'){
            $('#count-box').css('display', 'none')
        } else {
            $('#count-box').css('display', 'block')
        }
    })
</script>
@endpush

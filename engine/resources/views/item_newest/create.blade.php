@extends('layouts.master')
@section('main_content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Product Newest</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item active">add Product Newest</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{route('item_newest.store')}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="item_id">Product</label>
                                    <select class="form-control select2" name="item_id" id="item_id">
                                        @foreach ($items as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('item_id')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="variant_id">Variant</label>
                                    <select class="form-control select2" name="variant_id" id="variant_id">
                                    </select>
                                    @error('variant_id')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="variant-box col-12 row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="variant_discount_price">Discount Price</label>
                                            <input type="text" name="variant_discount_price" class="form-control"
                                                id="variant_discount_price" placeholder="Product discount_price"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                <label for="end_deal">Discount Start Date</label>
                                                <div class="input-group date timepicker" id="timepicker_start"
                                                    data-target-input="nearest">
                                                    <input type="text" name="variant_start_deal"
                                                        class="form-control datetimepicker-input"
                                                        id="variant_start_deal" data-target="#timepicker_start"
                                                        value="" />
                                                    <div class="input-group-append" data-target="#timepicker_start"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-clock"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                @error('end_deal')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                <label for="end_deal">Discount End Date</label>
                                                <div class="input-group date timepicker" id="timepicker_end"
                                                    data-target-input="nearest">
                                                    <input type="text" name="variant_end_deal" id="variant_end_deal"
                                                        class="form-control datetimepicker-input"
                                                        data-target="#timepicker_end" value="" />
                                                    <div class="input-group-append" data-target="#timepicker_end"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-clock"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                @error('end_deal')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('js')
<script>
    $( document ).ready(function() {
        $.fn.datetimepicker.Constructor.Default = $.extend({},
        $.fn.datetimepicker.Constructor.Default,
        { icons:{
            time: 'fas fa-clock',
            date: 'fas fa-calendar',
            up: 'fas fa-arrow-up',
            down: 'fas fa-arrow-down',
            previous: 'fas fa-arrow-circle-left'
            ,next: 'fas fa-arrow-circle-right',
            today: 'far fa-calendar-check-o',
            clear: 'fas fa-trash',
            close: 'far fa-times' } });
        $('.select2').select2({
            theme: 'bootstrap4',
        })
        $('.timepicker').datetimepicker({
            format: 'DD-MM-YYYY HH:mm'
        })
        $("#item_id").change();
    });

    $('#item_id').change(function(){
        var item_id = $('#item_id').find(":selected").val();
        $.ajax({
            url: `{{url('/')}}/get/`+item_id+`/item`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'get',
            success: function (data){
                $('#variant_id').empty()
                $.each(data, function(index, val){
                    $('#variant_id').append(`<option value="`+val.id+`" >`+val.variant+`</option>`)
                })
                $("#variant_id").change();
            }
        })
    })

    $('#variant_id').change(function(){
        var variant_id = $('#variant_id').find(":selected").val();
        $("#variant_discount_price").val('')
        $("#variant_start_deal").val('')
        $("#variant_end_deal").val('')
        $.ajax({
            url: `{{url('/')}}/get/`+variant_id+`/detail`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'get',
            success: function (data){
                $("#variant_discount_price").val(data.discount_price)
                $("#variant_start_deal").val(data.start_deal)
                $("#variant_end_deal").val(data.end_deal)
            }
        })
    })
</script>
@endpush

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemNewest extends Model
{
    use HasFactory;
    protected $guarded = [];

    function variants(){
        return $this->belongsTo('App\Models\ItemStockVariant', 'variant_id');
    }
}

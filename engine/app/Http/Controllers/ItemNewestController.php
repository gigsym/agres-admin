<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\ItemNewest;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ItemNewestController extends Controller
{
    public function index()
    {
        $d['items'] = ItemNewest::get();
        return view('item_newest.index', $d);
    }

    public function create()
    {
        $d['items'] = Item::all();
        return view('item_newest.create', $d);
    }

    public function store(Request $request)
    {
        ItemNewest::create([
            'variant_id' => $request->variant_id,
        ]);

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();
        return redirect(route('item_newest.index'))->with(['success' => " add new product success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['itemnewest'] = ItemNewest::find($id);
        $d['items'] = Item::all();
        return view('item_newest.edit', $d);
    }

    public function update(Request $request, $id)
    {

        $update = ItemNewest::find($id);
        $update->variant_id = $request->variant_id;
        $update->save();

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();

        return redirect(route('item_newest.index'))->with(['success' => " update product success"]);
    }

    public function destroy($id)
    {
        $item = ItemNewest::find($id);
        $item->delete();
        return redirect()->back()->with(['success' => " delete success"]);
    }
}

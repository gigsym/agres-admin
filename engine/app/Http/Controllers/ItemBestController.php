<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\ItemBest;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ItemBestController extends Controller
{
    public function index()
    {
        $d['items'] = ItemBest::get();
        return view('item_best.index', $d);
    }

    public function create()
    {
        $d['items'] = Item::all();
        return view('item_best.create', $d);
    }

    public function store(Request $request)
    {
        ItemBest::create([
            'variant_id' => $request->variant_id,
        ]);

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();
        return redirect(route('item_best.index'))->with(['success' => " add new product success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['itembest'] = ItemBest::find($id);
        $d['items'] = Item::all();
        return view('item_best.edit', $d);
    }

    public function update(Request $request, $id)
    {

        $update = ItemBest::find($id);
        $update->variant_id = $request->variant_id;
        $update->save();

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();

        return redirect(route('item_best.index'))->with(['success' => " update product success"]);
    }

    public function destroy($id)
    {
        $item = ItemBest::find($id);
        $item->delete();
        return redirect()->back()->with(['success' => " delete success"]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function index()
    {
        $d['vouchers'] = Voucher::get();
        return view('voucher.index', $d);
    }

    public function create()
    {
        $d['items'] = Item::select('id', 'name')->get();
        return view('voucher.create', $d);
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'code' => 'required',
        //     'discount_type' => 'required',
        //     'code' => 'required',
        //     'code' => 'required',
        // ]);
        if ($request->is_active == 'on') {
            $d['is_active'] = 1;
        } else {
            $d['is_active'] = 0;
        }
        $d['expiry_date'] = Carbon::parse($request->expiry_date);
        $request->merge($d);

        Voucher::create($request->except('_token'));
        return redirect(route('voucher.index'))->with(['success' => " add new voucher success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['voucher'] = Voucher::find($id);
        $d['items'] = Item::select('id', 'name')->get();
        return view('voucher.edit', $d);
    }

    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'name' => 'required',
        //     'email' => 'required|email',
        //     'phone' => 'required',
        // ]);
        if ($request->is_active == 'on') {
            $d['is_active'] = 1;
        } else {
            $d['is_active'] = 0;
        }
        $d['expiry_date'] = Carbon::parse($request->expiry_date);
        $request->merge($d);

        $voucher = Voucher::find($id);
        foreach($request->except('_token','_method') as $k => $v){
            $voucher->$k = $v;
        }
        $voucher->save();

        return redirect(route('voucher.index'))->with(['success' => " update voucher success"]);
    }

    public function destroy($id)
    {
        $voucher = Voucher::find($id);
        $voucher->delete();
        return redirect()->back()->with(['success' => " delete voucher success"]);
    }
}

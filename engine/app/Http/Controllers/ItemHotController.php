<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\ItemHot;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ItemHotController extends Controller
{
    public function index()
    {
        $d['items'] = ItemHot::with('variants')->get();
        return view('item_hot.index', $d);
    }

    public function create()
    {
        $d['items'] = Item::all();
        $d['variants'] = ItemStockVariant::all();
        return view('item_hot.create', $d);
    }

    public function store(Request $request)
    {
        ItemHot::create([
            'variant_id' => $request->variant_id,
        ]);

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();
        return redirect(route('item_hot.index'))->with(['success' => " add new product success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['itemhot'] = ItemHot::with('variants.product')->find($id);
        $d['items'] = Item::all();
        return view('item_hot.edit', $d);
    }

    public function update(Request $request, $id)
    {

        $update = ItemHot::find($id);
        $update->variant_id = $request->variant_id;
        $update->save();

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();

        return redirect(route('item_hot.index'))->with(['success' => " update product success"]);
    }

    public function destroy($id)
    {
        $category = ItemHot::find($id);
        $category->delete();
        return redirect()->back()->with(['success' => " delete category success"]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\ItemSelect;
use App\Models\ItemStockVariant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ItemSelectController extends Controller
{
    public function index()
    {
        $d['items'] = ItemSelect::with('variants.product')->get();
        return view('item_select.index', $d);
    }

    public function create()
    {
        $d['items'] = Item::all();
        return view('item_select.create', $d);
    }

    public function store(Request $request)
    {
        ItemSelect::create([
            'variant_id' => $request->variant_id,
        ]);

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();
        return redirect(route('item_select.index'))->with(['success' => " add new product success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['itemselect'] = ItemSelect::find($id);
        $d['items'] = Item::all();
        return view('item_select.edit', $d);
    }

    public function update(Request $request, $id)
    {

        $update = ItemSelect::find($id);
        $update->variant_id = $request->variant_id;
        $update->save();

        $variant = ItemStockVariant::find($request->variant_id);
        $variant->discount_price = $request->variant_discount_price;
        $variant->start_deal = Carbon::parse($request->variant_start_deal);
        $variant->end_deal = Carbon::parse($request->variant_end_deal);
        $variant->save();

        return redirect(route('item_select.index'))->with(['success' => " update product success"]);
    }

    public function destroy($id)
    {
        $category = ItemSelect::find($id);
        $category->delete();
        return redirect()->back()->with(['success' => " delete category success"]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\CmsPopupBanner;
use Illuminate\Http\Request;

class PopupBannerController extends Controller
{
    public function index()
    {
        $d['banners'] = CmsPopupBanner::all();
        return view('popup_banner.index', $d);
    }

    public function create()
    {
        return view('popup_banner.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'banner_desktop' => 'required',
            'banner_responsive' => 'required',
        ]);

        $store = new CmsPopupBanner;
        foreach ($request->except('_token','_method') as $i => $v) {
            if ($i == 'banner_desktop' || $i == 'banner_responsive') {
                $imageName = $v->getClientOriginalName();
                $imageNewName =  uniqid() . '_' . $imageName;
                $destinationPath = 'assets/images/cms/catalog/popup/';
                $v->move($destinationPath, $imageNewName);
                $path_file =  $destinationPath . $imageNewName;

                $store->$i = $path_file;
            } else {
                $store->$i = $v;
            }
        }
        $store->save();

        return redirect(route('popup_banner.index'))->with(['success' => "add popup banner success"]);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $d['banner'] = CmsPopupBanner::find($id);
        return view('popup_banner.edit', $d);
    }

    public function update(Request $request, $id)
    {
        $update = CmsPopupBanner::find($id);
        foreach ($request->except('_token','_method') as $i => $v) {
            if ($i == 'banner_desktop' || $i == 'banner_responsive') {
                $imageName = $v->getClientOriginalName();
                $imageNewName =  uniqid() . '_' . $imageName;
                $destinationPath = 'assets/images/cms/catalog/popup/';
                $v->move($destinationPath, $imageNewName);
                $path_file =  $destinationPath . $imageNewName;

                $update->$i = $path_file;
            } else {
                $update->$i = $v;
            }
        }
        $update->save();

        return redirect(route('popup_banner.index'))->with(['success' => "update popup banner success"]);
    }

    public function destroy($id)
    {
        $member = CmsPopupBanner::find($id);
        $member->delete();
        return redirect()->back()->with(['success' => "delete popup banner success"]);
    }

    public function updateStatus($banner_id)
    {
        $user = CmsPopupBanner::find($banner_id);
        if ($user->status == 1){
            $user->status = 0;
        } else {
            $user->status = 1;
        }
        $user->save();
        return back()->with(['success' => 'update popup status success']);
    }
}
